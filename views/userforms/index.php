
<title>Index</title>

<?php
include 'C:\xampp\htdocs\project\01-Cab-Booking\config\config.php';
include 'C:\xampp\htdocs\project\01-Cab-Booking\views\include\infonav.php';
include 'C:\xampp\htdocs\project\01-Cab-Booking\views\include\navbar.php';
?>

<div class="container">

     <div class="col-md-12 text-center">
            <img src="http://localhost/project/01-Cab-Booking/public/images/seating-plan.png" class="home-seating-plan">
      </div>
          <div class="col-md-12 product-search-title text-center">
           CHEAP PICK & DROP RATES
         </div>
            <div class="col-md-12 ">

                <div class="account-type">
                        <div>
                              <input type="radio" name="trip_type" id="one_way_trip" value="one_way_trip" class="account-type-radio" checked="" autocomplete="off">
                                <label for="one_way_trip" class="ripple-effect-dark">ONE WAY</label>
                        </div>
                      <div>
                              <input type="radio" name="trip_type" id="round_trip" value="round_trip" class="account-type-radio" autocomplete="off">
                                 <label for="round_trip" class="ripple-effect-dark">ROUND TRIP</label>
                      </div>
                </div>
             </div>

            <div class="d-none">
             <select class="form-control" name="vehicle" data-validation="required" id="car">
             <option value="1">Mercedes Vito (8 people, 3 cases, 5 hand luggage)</option>
             </select>
            </div>

             <div class="col-md-6 col-sm-6 ">
                       <label class="index-label">Pick up Point</label>
                         <select class="form-control" name="pick_up" id="get_place" data-dropoff="" data-validation="required">
                                  <option>Select</option>
                         </select>

                          </div>

                          <div class="col-md-6 col-sm-6 ">
                 <label class="index-label">Drop Off Point</label>
                 <select class="form-control" name="dropOff" id="addFilds" onchange="checkSamePointIndex()">
                   <option value="">Select</option>
                </select>
                  </div>

                <div class="col-md-12 d-none chose-diff-alert-div">
                     <div class="alert alert-danger index-diff-alert">
                        <a href="javascript:;" class="close">×</a>
                              Please select "Thurrock Area" either in pickup or in dropdown. You can also call us for booking on
                                <strong><a href="tel:00447763643366"> +447763643366</a> </strong>
                       </div>
                </div>

                        <div class="col-md-3">
                           <label class="index-label">No. of Passengers</label><br>
                                 <div class="input-group">
                                <input type="text" name="passengers" class="form-control qty" id="passengers" autocomplete="off">
                        </div>

                      </div>

                       <div class="col-md-3">
                            <label class="index-label">Payment Method</label><br>
                            <select class="form-control" name="paymentMethod" id="payments">
                              <option value="1">Cash to Driver</option>
                            </select>
                       </div>


                          <div class="col-md-4 ">
                            <label class="index-label">Box Trailer</label><br>
                               <div class="col-md-12">
                                <span><p>You will be charged 100rs addtionally.</p></span>
                                     <label class="radio-inline booking-trailor tooltips" data-tippy-content="You will be charged £60 additionally" tabindex="0">
                                     <input type="radio" name="box_trailer" id="box_trailer_yes" value="1" autocomplete="off"> Yes
                                        </label>
                                         <label class="radio-inline booking-trailor">
                                          <input type="radio" name="box_trailer" id="box_trailer_no" value="0" checked="" autocomplete="off"> No
                                           </label>
                                       </div>
                                   </div>

                         <div class="col-md-2 pt-2">
                            <br>
                            <button class="search-button pull-right btn transition-effect " id="showPrice">QUOTE NOW</button>
                           </div>
                  </div>


<section id="how-it-work">
        <div class="row work-row">
            <div class="container">
                <div class="section-title text-center">
                    <h2>HOW IT WORKS?</h2>
                    <h4>PRE-BOOK | SELECT | BOOK</h4>
                    <div class="space"></div>
                    <p>
                        Pre book a minibus in Thurrock/UK is simple using our secure online booking service. <b>Events Luxury Travel</b> offers private hires pick-up from <b>Thurrock</b> in the UK. <br>
                        <b>Events Luxury Travel</b> is operated 24 hours a day, 7 days a week and services in and around <b>Thurrock</b>.
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="work-step">
                    <div class="col-md-4 col-sm-4 col-xs-4 first-step text-center">
                        <i class="fa fa-search"></i>
                        <h5>PRE-BOOK</h5>
                        <p class="hide-onmobile">Search here the best available drive and the price</p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 second-step text-center">
                        <i class="fa fa-heart-o"></i>
                        <h5>SELECT</h5>
                        <p class="hide-onmobile">Select your car and pickup timings</p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 third-step text-center">
                        <i class="fa fa-shopping-cart"></i>
                        <h5>BOOK</h5>
                        <p class="hide-onmobile">Book your drive in just 3 minutes</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php include 'C:\xampp\htdocs\project\01-Cab-Booking\views\include\footer.php';?>
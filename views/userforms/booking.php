<title>Booking</title>
<?php
include 'C:\xampp\htdocs\project\01-Cab-Booking\config\config.php';
include 'C:\xampp\htdocs\project\01-Cab-Booking\views\include\infonav.php';
include 'C:\xampp\htdocs\project\01-Cab-Booking\views\include\navbar.php';

if (isset($_POST['submit'])) 
{   
         $trip_type      =    $_POST['trip_type'];
         $pickup_date   =    $_POST['pick_up_date'];
         $pickup_time   =    $_POST['pick_up_time'];
         $pickup_point   =    $_POST['pick_up'];
         $dropof_point    =    $_POST['drop_off'];
         $contact        =    $_POST['contact'];
         $passengers     =    $_POST['passengers'];
         $payment        =    $_POST['payment_method'];
         $box_trailer    =    $_POST['box_trailer'];
         $comments       =    $_POST['comments'];
         $first_name     =    $_POST['first_name'];
         $last_name      =    $_POST['last_name'];
         $address        =    $_POST['address'];
         $email          =    $_POST['email'];
         $postal_code    =    $_POST['postcode'];


if($crud->addbooking( $trip_type,$pickup_date,$pickup_time,$pickup_point,$dropof_point,$contact,$passengers,$payment,$box_trailer,$comments,$first_name,$last_name,$address,$email,$postal_code))
 {
  header("Location: booking.php?inserted");
 }
 else
 {
  // header("Location: index.php?failure");
 }

}
?>





<form method="POST">
 <div style="background-image: url('http://localhost/project/01-Cab-Booking/public/images/car.jpg');">
    <h2 class="container text-center" style="color:white;padding-top: 100px;padding-bottom: 100px;margin-top: 0px;"><b>Booking</b>
    </h2>
</div>
<div class="section-title text-center " style="margin-bottom: 0;">
         <h2>Book with the Best Private hire company in Thurrock</h2>
              <p>
                Pre booking your private hire minibus is simple using our secure online booking service. <b>Events Luxury Travel</b> offers minibus pick-up from <br><b>Thurrock</b> in the UK.
              </p>
</div>

<?php
if(isset($_GET['inserted']))
{
 ?>
    <div class="container">
 <div class="alert alert-info">
    <strong>WOW!</strong> Booking done successfully <a href="index.php">HOME</a>!
 </div>
 </div>
    <?php
}
else if(isset($_GET['failure']))
{
 ?>
    <div class="container">
 <div class="alert alert-warning">
    <strong>SORRY!</strong> ERROR while inserting record !
 </div>
 </div>
    <?php
}
?>
    

<div>
    <div class="col-md-8 col-sm-8 no-colpadding">
        <div class="passenger-detail">
            <h4><i class="fa fa-car"></i>Route Details</h4>
            <div class="passenger-detail-body">
            <div class="col-md-12">
                  <div class="account-type">
                        <div>
                            <input type="radio" name="trip_type" id="one_way_trip" value="one_way_trip" class="account-type-radio" checked autocomplete="off">
                            <label for="one_way_trip" class="ripple-effect-dark">ONE WAY</label>
                        </div>
                        <div>
                            <input type="radio" name="trip_type" id="round_trip" value="round_trip" class="account-type-radio" autocomplete="off">
                            <label for="round_trip" class="ripple-effect-dark">ROUND TRIP</label>
                        </div>
                    </div>
            </div>
        
        <div class="clearfix"></div>

            <div class="col-md-6">
                <label>Pick up Date</label>
                <input type="date" name="pick_up_date" class="form-control hasDatepicker" placeholder="MM/DD/YYYY" required>
            </div>
                                    
            <div class="col-md-3 col-xs-6">
                <label>Pick up Time</label>
                <input type="time"  name="pick_up_time" class="form-control hasDatepicker" required>
             


            </div>

            <div class="col-md-6">
             
                <label>Pick up Point</label>
                

                <?php

            try
            {
                 $sql = "select * from adressess";
                 $projresult = $DB_con->query($sql);                       
                 $projresult->setFetchMode(PDO::FETCH_ASSOC);


            }
            catch (PDOException $e)
            {   
                die("Some problem getting data from database !!!" . $e->getMessage());
            }
             ?>

            <select name="pick_up" class="form-control" >
               <option>Select</option>
            <?php
            while ( $row = $projresult->fetch() ) 
            {?>
               <option><?= $row['pickup_points'];  ?></option>
            <?php
            }

            ?>
          </select>




            </div>

            <div class="col-md-6">
                <label>Drop off Point</label>
                
                <?php

            try
            {
                 $sql = "select * from adressess";
                 $projresult = $DB_con->query($sql);                       
                 $projresult->setFetchMode(PDO::FETCH_ASSOC);


            }
            catch (PDOException $e)
            {   
                die("Some problem getting data from database !!!" . $e->getMessage());
            }
             ?>

            <select name="drop_off" class="form-control" >
               <option>Select</option>
            <?php
            while ( $row = $projresult->fetch() ) 
            {?>
               <option><?= $row['dropof_points'];  ?></option>
            <?php
            }

            ?>
          </select>





            </div>

            <div class="col-md-3 col-xs-6 ">
                <label>No. of Passengers</label>
                <div class="input-group">
                    <input type="text" value="1" name="passengers" required class="form-control qty">
                </div>
            </div>
            
            <div class="col-md-3 col-xs-6">
                <label>Payment Method</label>
                <select class="form-control" name="payment_method" required >
                        <option value="1">Cash to Driver</option>
                </select>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-6">
                <label>Box Trailer</label><br>
                <div class="col-md-12 p-0 mt-8">
                <input type="radio" value="yes" name="box_trailer"> Yes
                <label class="radio-inline booking-trailor">
                <input type="radio" value="no" name="box_trailer" checked>No
                </label>
                </div>
            </div>
                    
            <div class="col-md-12">
                <label>Comments/Notes</label>
            <textarea class="form-control text-box" name="comments" ></textarea>
            </div>


            <div class="col-md-12">
                <div class="form-group fare-calculate">
                    <b>Please choose desired route</b>
                </div>
            </div>
        </div>
    </div>
        
        <div class="passenger-detail">
            <h4><i class="fa fa-user"></i>Your Personal Details</h4>
            <div class="passenger-detail-body">
                <div class="col-md-3 ol-sm-3">
                    <label>First Name</label>
                    <input type="text" class="form-control" name="first_name" placeholder="First Name *" required>
                </div>
            <div class="col-md-3 ol-sm-3">
                <label>Last Name</label>
                <input type="text" class="form-control" name="last_name" placeholder="Last Name *" required>
            </div>
                                    <div class="col-md-6 ol-sm-6">
                                        <label>Contact Number</label>
                                        <input type="text" class="form-control" name="contact" required placeholder="Contact Number *">
                                    </div>
                                    <div class="col-md-3 ol-sm-6">
                                        <label>Postal Code</label>
                                        <i class="fa fa-spinner fa-spin" style="display: none;"></i>
                                        <input type="text" class="form-control" name="postcode" placeholder="Postal Code *" required>
                                    </div>
                                    <div class="col-md-9 ol-sm-6" id="fillAddress">
                                        <label>Address</label>
                                        <input type="text" class="form-control" name="address" placeholder="Address *" required>
                                    </div>
                                    <div class="col-md-6 ol-sm-6">
                                        <label>Your Email</label>
                                        <input type="email" class="form-control" name="email" placeholder="Email Address *" required>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 ol-sm-6">
                                        <label>&nbsp;</label>
                                        <label>
                                            <input type="checkbox" name="terms" value="1" required  autocomplete="off" data-parsley-multiple="terms" data-parsley-id="64">
                                            I accept the <a href="https://eventsluxurytravel.com/terms-conditions" target="_blank">Terms and Conditions</a>
                                        </label>
                                    </div>
                                    <div class="text-center col-sm-12">
                                        <button type="submit" name="submit">Book This! </button>
                                    </div>

                                </div>
                            </div>
                        </div>
</div>

</form>
<div class="col-md-4 col-sm-4 booking-sidebar">
                        <div class="sidebar-item">
                            <h4><i class="fa fa-phone"></i>Need Help?</h4>
                            <div class="sidebar-body text-center">
                                <a href="https://web.whatsapp.com/send?phone=+447763643366"><img src="http://localhost/project/01-Cab-Booking/public/images/24hours.png" class="whattsapp_img"></a>
                            </div>
                        </div>
                        <div class=" clear-padding text-center">
                            <img src="http://localhost/project/01-Cab-Booking/public/images//seating-plan-rotate.png" class="img-responsive booking-img">
                        </div>
                    </div>
</div>

<div class="clearfix"></div>
<?php include 'C:\xampp\htdocs\project\01-Cab-Booking\views\include\footer.php';?>
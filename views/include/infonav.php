<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $title; ?></title>

  
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="https://eventsluxurytravel.com/public/cruise/assets/css/style.css" rel="stylesheet" media="screen">
  <link href="http://localhost/project/01-Cab-Booking/styles/style.css" rel="stylesheet" media="screen">
</head>
<body>
<div class="row transparent-menu-top">
        <div class="container clear-padding">
            <div class="navbar-contact">
                <div class="col-md-7 col-sm-6 clear-padding">
                    <a href="https://www.facebook.com/eventsluxurytravel/" target="blank" class="transition-effect"><i class="fa-brands fa-facebook-f"></i>  @eventsluxurytrave</a>
                    <a href="tel:00447763643366" class="transition-effect"><i class="fa fa-phone"></i> 03467775003</a>
                </div>
               
            </div>
        </div>
    </div>

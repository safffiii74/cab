
<section id="footer">
    <footer>
        <div class="row main-footer-sub">
            <div class="container clear-padding">
                <div class="section-title pages-section text-center">
                    <h2>Areas Covered by our Car Service</h2>
                    <p>
                        <b>Events Luxury Travel</b> is operated 24 hours a day, 7 days a week and services in and around <b>Thurrock</b>.
                    </p>
                </div>
                <div class="col-md-12 col-sm-12">
                                    </div>
            </div>
        </div>
        <div class="main-footer row">
            <div class="container clear-padding">
                <div class="col-md-6 col-sm-6 about-box">
                    <a class="footer-logo p-0" href="#">
                        <img src="http://localhost/project/01-Cab-Booking/public/images/logo.png">
                    </a>
                    <p class="text-justify"><b>Events Luxury Travel</b> Established in oct 2016 with a very few vehicles, the brand now owns a well-maintained fleet of economy, luxury and commercial vehicles in UK. We provide experienced chauffeurs & drivers with vehicles on request.
                        <b>Events Luxury Travel</b> is operated 24 hours a day, 7 days a week and services in and around Thurrock. Book your private hire minibus now with <b>Events Luxury Travel</b>.</p>
                </div>

                <div class="visible-sm-block"></div>
                <div class="col-md-2 col-sm-6 links ">
                    <h4>UseFul Links</h4>
                    <ul class="">
                        <p><i class="fa fa-angle-double-right"></i> <a href="#">Home</a></p>
                        <p><i class="fa fa-angle-double-right"></i> <a href="#">Booking</a></p>
                        <p><i class="fa fa-angle-double-right"></i> <a href="#">Gallery</a></p>
                        <p><i class="fa fa-angle-double-right"></i> <a href="#">Contact</a></p>
                        <p><i class="fa fa-angle-double-right"></i> <a href="#">About</a></p>
                        <p><i class="fa fa-angle-double-right"></i> <a href="#">Tell a Friend</a></p>
                    </ul>

                </div>
                <div class="col-md-4 col-sm-6 contact-box">
                    <h4>Contact Us</h4>
                    <p>
                        <i class="fa fa-home"></i><a href="javascript:;"> 102 Victoria Avenue Grays RM16 2RW</a>
                    </p>
                    <p><i class="fa fa-phone"></i><a href="tel:00447763643366"> +44 7763 643366</a></p>
                    <p><i class="fa fa-envelope"></i><a href="#"> eventsluxurytravel@gmail.com</a></p>
                    <p><i class="fa-brands fa-facebook"></i><a href="#" target="blank"> @eventsluxurytravel</a></p>
                </div>
            </div>
        </div>
        <div class="main-footer-nav row">
            <div class="container clear-padding">
                <div class="col-md-12 col-sm-12">
                    <p class="text-center">Copyright © 2022 Events  Luxury Travel | All rights reserved | Designed & Developed by  <a href="#">Top in search</a></p>
                </div>

                <div class="go-up">
                    <a href="#"><i class="fa fa-arrow-up"></i></a>
                </div>
            </div>
        </div>
    </footer>
</section>
</body>
</html>
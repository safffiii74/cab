<?php

class Crud
{
 private $db;
 
 function __construct($DB_con)
 {
  $this->db = $DB_con;
 }
 

public function addbooking( $trip_type,$pickup_date,$pickup_time,$pickup_point,$dropof_point,$contact,$passengers,$payment,$box_trailer,$comments,$first_name,$last_name,$address,$email,$postal_code)
 {
  try
  {
   $stmt = $this->db->prepare("INSERT INTO booking (trip_type,pickup_date,pickup_time,pickup_point,dropof_point,contact,passengers,payment,box_trailer,comments,first_name,last_name,address,email,postal_code ) 
      VALUES(:trip_type,:pickup_date,:pickup_time,:pickup_point,:dropof_point,:contact,:passengers,:payment,:box_trailer,:comments,:first_name,:last_name,:address,:email,:postal_code)");
   


   $stmt->bindparam(":trip_type" , $trip_type);
   $stmt->bindparam(":pickup_date" , $pickup_date);
 
   $stmt->bindparam(":pickup_time" , $pickup_time);



   $stmt->bindparam(":pickup_point",$pickup_point);
   $stmt->bindparam(":dropof_point",$dropof_point);
   $stmt->bindparam(":contact",$contact);
      $stmt->bindparam(":passengers",$passengers);
   $stmt->bindparam(":payment",$payment);
      $stmt->bindparam(":box_trailer",$box_trailer);
         $stmt->bindparam(":comments",$comments);
   $stmt->bindparam(":first_name",$first_name);
   $stmt->bindparam(":last_name",$last_name);
   $stmt->bindparam(":address",$address);
   $stmt->bindparam(":email",$email);
   $stmt->bindparam(":postal_code",$postal_code);
   $stmt->execute();
   return true;
  }
  catch(PDOException $e)
  {
   echo $e->getMessage(); 
   return false;
  }
  
 }














public function dataview($query)
 {
  $stmt = $this->db->prepare($query);
  $stmt->execute();
 
  if($stmt->rowCount()>0)
  {
   while($row=$stmt->fetch(PDO::FETCH_ASSOC))
   {
    ?>
                <tr>

                <td><?php print($row['id']); ?></td>
                <td><?php print($row['rate']); ?></td>
                </tr>
                <?php
   }
  }
  else
  {
   ?>
            <tr>
            <td colspan="3" align="center">No Record Found</td>
            </tr>
            <?php
  }
  
 }
 
 public function paging($query,$records_per_page)
 {
  $starting_position=0;
  if(isset($_GET["page_no"]))
  {
   $starting_position=($_GET["page_no"]-1)*$records_per_page;
  }
  $query2=$query." limit $starting_position,$records_per_page";
  return $query2;
 }
 
 public function paginglink($query,$records_per_page)
 {
  
  $self = $_SERVER['PHP_SELF'];
  
  $stmt = $this->db->prepare($query);
  $stmt->execute();
  
  $total_no_of_records = $stmt->rowCount();
  
  if($total_no_of_records > 0)
  {
   ?><ul class="pagination"><?php
   $total_no_of_pages=ceil($total_no_of_records/$records_per_page);
   $current_page=1;
   if(isset($_GET["page_no"]))
   {
    $current_page=$_GET["page_no"];
   }
   if($current_page!=1)
   {
    $previous =$current_page-1;
    echo "<li><a href='".$self."?page_no=1'>First</a></li>";
    echo "<li><a href='".$self."?page_no=".$previous."'>Previous</a></li>";
   }
   for($i=1;$i<=$total_no_of_pages;$i++)
   {
    if($i==$current_page)
    {
     echo "<li><a href='".$self."?page_no=".$i."' style='color:red;'>".$i."</a></li>";
    }
    else
    {
     echo "<li><a href='".$self."?page_no=".$i."'>".$i."</a></li>";
    }
   }
   if($current_page!=$total_no_of_pages)
   {
    $next=$current_page+1;
    echo "<li><a href='".$self."?page_no=".$next."'>Next</a></li>";
    echo "<li><a href='".$self."?page_no=".$total_no_of_pages."'>Last</a></li>";
   }
   ?></ul><?php
  }
 }
 

 
}
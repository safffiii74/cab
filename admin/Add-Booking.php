<?php 
include 'C:\xampp\htdocs\project\01-Cab-Booking\config\config.php';
session_start();

if (isset($_POST['book'])) 

        {

         $customerid = $_POST['customer-id'];
         $source = $_POST['source'];
         $destination = $_POST['destination'];
         $fare = $_POST['fare']; 
         $date= $_POST['date'];

         $sql = "INSERT INTO reservations(customer_id,source,destination,fare,date) VALUES(:customerid,:source,:destination,:fare,:date)";
         $prepare_sql = $conn->prepare($sql);

         $data= 
         [
            ':customerid' => $customerid ,
            ':source' => $source ,
            ':destination' => $destination,
            ':fare' => $fare ,
            ':date' =>$date,
         ];

         $run_sql = $prepare_sql->execute($data);

            if ($run_sql) {
                $_SESSION['message'] = "Booking Done Successfully!!";
                header("location:Bookings.php");
                exit(0);
            }
            else
            {
                $_SESSION['message'] = "Booking Not Done ";
                header("location:Bookings.php");
                exit(0);
            }



        }




 ?>
 <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
    <title>Add New Booking</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-8 mt-4">
                <div class="card">
                    <div class="card-header">
                        <h3>Add New Booking  
                            <a href="Bookings.php" class="btn btn-danger float-end">Back</a>
                        </h3>
                    </div>
                    <div class="card-body">
                        <form method="POST">
                            <div class="mb-3">
                                <label>Customer ID</label>
                                <input type="text" name="customer-id" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label>Source</label>
                                <input type="text" name="source" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label>Destination</label>
                                <input type="text" name="destination" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label>Fare</label>
                                <input type="text" name="fare" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label>Date</label>
                                <input type="text" name="date" class="form-control">
                            </div>
                            <div class="mb-3" style="text-align: center;" >
                                <button class="btn btn-danger" type="submit" name="book">Book Now</button>
                            </div>
                        </form>
                        
                    </div>
                </div>

            </div>
    </div>
</body>
</html>
<?php 
include 'C:\xampp\htdocs\project\01-Cab-Booking\config\config.php';
session_start();
 ?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Mileage Quoter</title>
	<link href="css/navbar style.css" rel="stylesheet" type="text/css" />
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
	<?php 
	include "navbar.php";
	 ?>
	<div class="container" style="margin-top: 125px;">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
<div>
							<?php if(isset($_SESSION['message'])) : ?>
							<h3 class="alert alert-success"><?= $_SESSION['message']; ?></h3>
							<?php 
							unset($_SESSION['message']);
							endif; ?>
						</div>
					<div class="card-body">
						<table class="table table-bordered table-striped table-hover">
							<thead>	
								<tr>
								
									<th>ID</th>
									<th>Destination</th>
									<th>Fare</th>
									<th colspan="2" style="text-align:center" >Action</th>
								</tr>
							</thead>
							 <tbody>
								<?php 
								$query = "select * from fare ";
								$statement = $conn->prepare($query);
								$statement->execute();
								$result = $statement->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_ASSOC

								if ($result)
								 {
									foreach ($result as $row) {
										?>
										<tr>
											<td><?= $row['id'];  ?></td>
											<td><?= $row['destination'];  ?></td>
											<td><?= $row['one_way_fare'];  ?></td>
											<td>
												<a class="btn btn-primary" href="edit-mileage.php?id=<?= $row['id']?>" >Edit</a>
											</td>
											<td>
												<form action="delete-mileage.php" method="POST">
													<button type="submit" name="delete" value="<?= $row['id']; ?>" class="btn btn-danger">Delete</button>
													
												</form>
											</td>
										</tr>

										<?php 
									}
								 }
								 else
								 {
								 	?>
								 	<tr>
								 		<td colspan="3" style="text-align:center;"><b>No data Found</b></td>
								 	</tr>
								 	<?php 
								 }

								 ?>
							</tbody>
 
						</table>		
					</div>

	<?php 
				include 'C:\xampp\htdocs\project\01-Cab-Booking\views\userforms\footer .php';
	 ?>
</body>
</html>
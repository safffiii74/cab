<?php 
include 'C:\xampp\htdocs\project\01-Cab-Booking\config\config.php';
session_start();
if (isset($_POST['update'])) {
		 
		 $id = $_POST['id'];	
		 $destination = $_POST['destination'];
         $fare = $_POST['fare']; 
        

		 try 
		 {
		 	// UPDATE fare SET  destination=:destination, one_way_fare= :fare where id = :id
		 	$query = "UPDATE `fare` SET `destination`=:destination,`one_way_fare`=:one_way_fare WHERE `id`=:id";
		 	$statement =  $conn->prepare($query);

		 	$data=[
		 	':destination' => $destination,
            ':fare' => $fare,
            ':id'=>$id,	
		 	];

		 	$query_execute = $statement->execute($data);
		 	if ($query_execute) 
		 	{
		 		$_SESSION['message'] = "Fare is Updated Successfully";
		 		header("location:Mileage Quoter.php");
		 		exit(0);
		 	}
		 	else
		 	{
		 	$_SESSION['message'] = "Fare is Not Updated ";
		 		header("location:Mileage Quoter.php");
		 		exit(0);	
		 	}

		 }
		  catch (PDOException $e) 
		 {
		 	echo $e->getMessage();	
		 }


}

 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	
	<title>Edit Fare</title>
</head>
<body>
	<?php 
	include "navbar.php";
	 ?>

	<div class="container" style="margin-top: 125px";>
		<div class="row">
			<div class="col-md-8 mt-4">
				<div class="card">
					<div class="card-header">
						<h3>Edit Fare  
							<a href="Mileage Quoter.php" class="btn btn-danger float-end">Back</a>
						</h3>
					</div>
					<div class="card-body">
						<?php 
						if (isset($_GET['id'])) {
							$id = $_GET['id'];
							$sql = "SELECT * FROM fare WHERE id = :id";
							$statement = $conn->prepare($sql);
							$data=[':id'=> $id ];
							$statement->execute($data);
							$result = $statement->fetch(PDO::FETCH_ASSOC); 
							// PDO::FETCH_ASSOC //PDO::FETCH_OBJ

						}
						 ?>
						<form method="POST">
							<input type="hidden" name="id" value=" <?= $result['id']; ?>">
							
							
							
							
							<div class="mb-3">
								<label>Destination</label>
								<input type="text" name="destination" value=" <?= $result['destination']; ?>" class="form-control">
							</div>
							<div class="mb-3">
								<label>Fare</label>
								<input type="text" name="fare" value=" <?= $result['one_way_fare']; ?>" class="form-control">
							</div>
														
							<div class="mb-3" style="text-align: center;" >
								<button class="btn btn-danger" type="submit" name="update">Update</button>
							</div>
						</form>
						
					</div>
				</div>

			</div>
	</div>
	<?php 
	include 'C:\xampp\htdocs\project\01-Cab-Booking\views\userforms\footer .php';
	 ?>
</body>
</html>
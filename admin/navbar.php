<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="text/html; charset=windows-1252">
        <title>Events Luxury Travel</title>
        <link href="css/navbar style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
    <header id="topnav">
        <div class="topbar-main">
            <div class="container">
                <div class="topbar-left">
                    <a href="https://eventsluxurytravel.com/admin/mange-booking" class="logo">
                        <h3 style="margin-top: 15px; margin-bottom:18px;">Events Luxury Travel</h3>
                    </a>
                </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="navbar-custom">
    <div class="container">
        <div id="navigation">
            <ul class="navigation-menu">
                <li>
                    <a href="Dashboard.php"> <span>Dashboard</span> </a>
                </li>
                <li>
                    <a href="Bookings.php"> <span>Bookings</span> </a>
                </li>
                        <li>
                            <a href="Addresses.php"> <span>Addresses</span> </a>
                        </li>
                        <li>
                            <a href="Manage Staff.php">Manage Staff</a>
                        </li>
                        <li>
                            <a href="Events.php"> <span>Events</span> </a>
                        </li>
                        <li>
                            <a href="Mileage Quoter.php"> <span>Mileage Quoter</span> </a>
                        </li>
                        <li class="has-submenu">
                            <a href="#"><span> Management <i class="fa fa-caret-down"></i></span> </a>
                            <ul class="submenu">
                                 <li>
                            <a href="Manage Contracts.php"> Manage Contracts</a>
                        </li>
                        <li>
                            <a href="Manage Contracts Prices.php"> Manage Contracts Prices</a>
                     </li>
                              <li>
                                <a href="Rates.php">Rates</a>
                            </li>
                            <li>
                                <a href="Pages.php"> Pages</a>
                            </li>
                            <li>
                                <a href="Gallery.php"> Gallery  </a>
                            </li>
                            <li>
                                <a href="Promotions.php"> Promotions </a>
                            </li>
                            <li>
                                <a href="Manage Fleet.php">Manage Fleet </a>
                            </li>
                            <li>
                                <a href="Manage Seats.php"> Manage Seats</a>
                            </li>
                            <li>
                                <a href="Email Templates.php"> Email Templates </a>
                            </li>
                            <li>
                                <a href="Payment Method.php">  Payment Method</a>
                            </li>
                            <li>
                                <a href="Contact Request.php">Contact Request </a>
                            </li>
                            <li>
                                <a href="#"> Manage Staff Invoices</a>
                            </li>
                                <li>
                                    <a href="#"> Manage Contract Invoices</a>
                                </li>
                        </ul>
                    </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
</body>
</html>
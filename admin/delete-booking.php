<?php 
session_start();
include 'C:\xampp\htdocs\project\01-Cab-Booking\config\config.php';

if (isset($_POST['delete'])) 
{
	$id= $_POST['delete'];
	try {
			$sql = "DELETE FROM reservations where reservation_id = :id";
			$statement= $conn->prepare($sql);
			$data = [':id'=> $id];
			$query_execute = $statement->execute($data);

			if ($query_execute) {
		 		$_SESSION['message'] = "Data Deleted Successfully";
		 		header("location:Bookings.php");
		 		exit(0);				
			}
			else 
			{
		 		$_SESSION['message'] = "Data not Deleted";
		 		header("location:Bookings.php");
		 		exit(0);				
			}

		} catch (PDOException $e) 
		{
			echo $e->getMessage();	
		}	
}


 ?>
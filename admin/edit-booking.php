<?php 
include 'C:\xampp\htdocs\project\01-Cab-Booking\config\config.php';
session_start();
if (isset($_POST['update'])) {
		 
		 $id = $_POST['id'];	
         $customerid = $_POST['customer-id'];
         $source = $_POST['source'];
         $destination = $_POST['destination'];
         $fare = $_POST['fare']; 
         $date= $_POST['date'];	

		 try 
		 {
		 	$query = "UPDATE reservations SET customer_id= :customerid ,source= source, destination=:destination, fare= :fare where reservation_id = :id";
		 	$statement =  $conn->prepare($query);

		 	$data=[
		 	':customerid' => $customerid ,
            ':source' => $source ,
            ':destination' => $destination,
            ':fare' => $fare ,
            ':date' =>$date,
		 	':id'=>$id,	
		 	];

		 	$query_execute = $statement->execute($data);
		 	if ($query_execute) 
		 	{
		 		$_SESSION['message'] = "Booking is Updated Successfully";
		 		header("location:Bookings.php");
		 		exit(0);
		 	}
		 	else
		 	{
		 	$_SESSION['message'] = "Booking is Not Updated ";
		 		header("location:Bookings.php");
		 		exit(0);	
		 	}

		 }
		  catch (PDOException $e) 
		 {
		 	echo $e->getMessage();	
		 }


}

 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	
	<title>Edit Student</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-8 mt-4">
				<div class="card">
					<div class="card-header">
						<h3>Edit Booking  
							<a href="Bookings.php" class="btn btn-danger float-end">Back</a>
						</h3>
					</div>
					<div class="card-body">
						<?php 
						if (isset($_GET['id'])) {
							$id = $_GET['id'];
							$sql = "SELECT * FROM reservations WHERE reservation_id = :id";
							$statement = $conn->prepare($sql);
							$data=[':id'=> $id ];
							$statement->execute($data);
							$result = $statement->fetch(PDO::FETCH_ASSOC); 
							// PDO::FETCH_ASSOC //PDO::FETCH_OBJ

						}
						 ?>
						<form method="POST">
							<input type="hidden" name="id" value=" <?= $result['reservation_id']; ?>">
							
							<div class="mb-3">
								<label>Customer ID</label>
								<input type="text" name="customer-id" value="<?= $result['customer_id']; ?>"  class="form-control">
							</div>
							
							<div class="mb-3">
								<label>Source</label>
								<input type="text" name="source" value="<?= $result['source']; ?>" class="form-control">
							</div>
							<div class="mb-3">
								<label>Destination</label>
								<input type="text" name="destination" value=" <?= $result['destination']; ?>" class="form-control">
							</div>
							<div class="mb-3">
								<label>Fare</label>
								<input type="text" name="fare" value=" <?= $result['fare']; ?>" class="form-control">
							</div>
							<div class="mb-3">
								<label>Date</label>
								<input type="text" name="date" value=" <?= $result['date']; ?>" class="form-control">
							</div>							
							<div class="mb-3" style="text-align: center;" >
								<button class="btn btn-danger" type="submit" name="update">Update</button>
							</div>
						</form>
						
					</div>
				</div>

			</div>
	</div>
</body>
</html>